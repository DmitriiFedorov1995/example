{
    "priority": [
        "\"${priority}\" is not a valid choice."
    ],
    "queue": [
        "Incorrect type. Expected pk value, received str."
    ]
}