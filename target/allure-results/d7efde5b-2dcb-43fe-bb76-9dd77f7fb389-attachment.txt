{
    "error": "0",
    "total": "1243",
    "page": "1",
    "books": [
        {
            "title": "Just Spring Integration",
            "subtitle": "A Lightweight Introduction to Spring Integration",
            "isbn13": "9781449316082",
            "price": "$16.99",
            "image": "https://itbook.store/img/books/9781449316082.png",
            "url": "https://itbook.store/books/9781449316082"
        },
        {
            "title": "Spring Integration in Action",
            "subtitle": "",
            "isbn13": "9781935182436",
            "price": "$31.86",
            "image": "https://itbook.store/img/books/9781935182436.png",
            "url": "https://itbook.store/books/9781935182436"
        },
        {
            "title": "Just Spring",
            "subtitle": "A lightweight introduction to the Spring Framework",
            "isbn13": "9781449306403",
            "price": "$15.18",
            "image": "https://itbook.store/img/books/9781449306403.png",
            "url": "https://itbook.store/books/9781449306403"
        },
        {
            "title": "Just Spring Data Access",
            "subtitle": "Covers JDBC, Hibernate, JPA and JDO",
            "isbn13": "9781449328382",
            "price": "$15.82",
            "image": "https://itbook.store/img/books/9781449328382.png",
            "url": "https://itbook.store/books/9781449328382"
        },
        {
            "title": "Pro Spring Integration",
            "subtitle": "",
            "isbn13": "9781430233459",
            "price": "$36.80",
            "image": "https://itbook.store/img/books/9781430233459.png",
            "url": "https://itbook.store/books/9781430233459"
        },
        {
            "title": "Pivotal Certified Spring Enterprise Integration Specialist Exam",
            "subtitle": "A Study Guide",
            "isbn13": "9781484207949",
            "price": "$49.99",
            "image": "https://itbook.store/img/books/9781484207949.png",
            "url": "https://itbook.store/books/9781484207949"
        },
        {
            "title": "Spring Boot 2 Recipes",
            "subtitle": "A Problem-Solution Approach",
            "isbn13": "9781484239629",
            "price": "$32.77",
            "image": "https://itbook.store/img/books/9781484239629.png",
            "url": "https://itbook.store/books/9781484239629"
        },
        {
            "title": "Spring REST",
            "subtitle": "Rest and web services development using Spring",
            "isbn13": "9781484208243",
            "price": "$47.10",
            "image": "https://itbook.store/img/books/9781484208243.png",
            "url": "https://itbook.store/books/9781484208243"
        },
        {
            "title": "Mockito for Spring",
            "subtitle": "Learn all you need to know about the Spring Framework and how to unit test your projects with Mockito",
            "isbn13": "9781783983780",
            "price": "$24.99",
            "image": "https://itbook.store/img/books/9781783983780.png",
            "url": "https://itbook.store/books/9781783983780"
        },
        {
            "title": "Spring MVC Cookbook",
            "subtitle": "Over 40 recipes for creating cloud-ready Java web applications with Spring MVC",
            "isbn13": "9781784396411",
            "price": "$54.99",
            "image": "https://itbook.store/img/books/9781784396411.png",
            "url": "https://itbook.store/books/9781784396411"
        }
    ]
}